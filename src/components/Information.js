import React, { Component } from 'react'

import '../css/Information.css'

export default class Information extends Component {
  // constructor(props) {
  //   super(props)
  // }

  render() {
    return (
      <div className="information">
        <h3 className="title">Informationen</h3>
        <hr className="title-hr" />

        <h5 className="title-small">Entwickelt von:</h5>
        <span>
          <ul>
            <li>Jannik Wibker</li>
            <li>Paul Brauckmann</li>
            <li>Magnus Joda</li>
          </ul>
        </span>
        <h5 className="title-small">Designed von:</h5>
        <span>
          <ul>
            <li>Anna Uphoff</li>
            <li>Sailaxman Muraleetharan</li>
          </ul>
        </span>
        <h5 className="title-small">Programmiersprachen:</h5>
        <span>
          <ul>
            <li>javascript</li>
            <li>css</li>
            <li>html5</li>
            <li>php</li>
            <li>sql</li>
          </ul>
        </span>
        <h5 className="title-small">wir benutzen:</h5>
        <span>
          <ul>
            <li>react</li>
            <li>react router</li>
            <li>skeleton css</li>
            <li>mySQL</li>
          </ul>
        </span>
      </div>
    )
  }
}
