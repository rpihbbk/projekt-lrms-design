import React, { Component } from 'react'
import logo from '../logo.svg'
import '../css/Header.css'

export default class Header extends Component {
  constructor() {
    super()
    console.log('')
  }

  render() {
    return (
      <div className="Header">
        <span className="hbbk-title">Hans-Böckler-Berufskolleg</span>
        <img src={logo} className="logo" alt="logo" />
        <img src={'/hbbk.png'} className="hbbk-logo" alt="hbbk" />
      </div>
    )
  }
}
