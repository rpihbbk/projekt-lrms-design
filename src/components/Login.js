import React, { Component } from 'react'
import '../css/Login.css'

import InputField from './InputField'

export default class Login extends Component {
  constructor() {
    super()

    this.login = this.login.bind(this)

    this.username_field = {
      error: '',
      type: 'text',
      placeholder: 'Benutzername',
      title: 'Benutzername:',
      cb: el => {
        this.username_field.el = el
      }
    }
    this.password_field = {
      error: '',
      type: 'password',
      placeholder: 'Passwort',
      title: 'Passwort:',
      cb: el => {
        this.password_field.el = el
      }
    }
  }

  login() {
    let username = this.username_field.el.value
    let password = this.password_field.el.value
    //console.log(username, password)
    this.props.history.push('/interface')
  }

  render() {
    return (
      <div className="Login">
        <div className="login-area">
          <InputField type={this.username_field.type} title={this.username_field.title} placeholder={this.username_field.placeholder} error={this.username_field.error} cb={this.username_field.cb} />
          <InputField type={this.password_field.type} title={this.password_field.title} placeholder={this.password_field.placeholder} error={this.password_field.error} cb={this.password_field.cb} />
          <span className="login-btn" onClick={this.login}>Login</span>
        </div>
      </div>
    )
  }
}
