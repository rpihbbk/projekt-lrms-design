import React, { Component } from 'react'

export default class DayView extends Component {
  constructor() {
    super()
    this.date = new Date() // new Date(2017, 2, 19)
  }

  render() {
    return (
      <div className="DayView">
        {this.date.getDate()}
      </div>
    )
  }
}
