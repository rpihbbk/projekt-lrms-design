import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import '../css/SideBar.css'

export default class SideBar extends Component {
  // constructor(props) {
  //   super(props)
  // }

  render() {
    return (
      <div className="SideBar">
        <div>
          {this.props.views.map((view, i) => {
            return view.url
                ? (<Link to={view.url} key={i} className={`item ${view.className}${this.props.active === view.id
                  ? ' active'
                  : ''}`}>
                  <i className="material-icons">{view.icon}</i>
                  <span style={{paddingLeft: view.icon ? '0px' : '24px'}}>{view.name}</span>
                </Link>)

                : (<span key={i} className={`item ${view.className}${this.props.active === view.id
                  ? ' active'
                  : ''}`}>
                  <i className="material-icons">{view.icon}</i>
                  <span style={{paddingLeft: view.icon ? '0px' : '24px'}}>{view.name}</span>
                </span>)
          })}
        </div>
      </div>
    )
  }
}
