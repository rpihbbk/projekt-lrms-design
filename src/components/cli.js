import React, { Component } from 'react'

import Terminal from './Terminal.js'

import '../css/cli.css'

export default class CLI extends Component {
  // constructor(props) {
  //   super(props)
  // }

  render() {
    return (
      <div className="cli"><Terminal headless={true} /></div>
    )
  }
}
