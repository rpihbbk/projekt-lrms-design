import React from 'react'
import '../css/InputField.css'

export default (props) => (
    <div className="InputField">
      {props.title
        ? <span className="title">{props.title}</span>
        : null
      }
      <input type={props.type} placeholder={props.placeholder} className="input" ref={props.cb} />
      {props.small ? null : <br />}
      {props.small ? null : <span className="error">{props.error}</span>}
      {props.small ? null : <br />}
    </div>
  )
