import React, { Component } from 'react'

export default class WeekView extends Component {
  constructor() {
    super()
    this.date = new Date() // new Date(2017, 2, 19)
    this._startOfWeek = this.date.getDate() - (this.date.getDay() === 0 ? 6 : this.date.getDay() - 1)
    this._endOfWeek = this._startOfWeek + 6
    this.startOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this._startOfWeek)
    this.endOfWeek = new Date(this.date.getFullYear(), this.date.getMonth(), this._endOfWeek)
  }

  render() {
    return (
      <div className="WeekView">
        {[0,1,2,3,4,5,6].map(i => (
          <span key={i}>{
            new Date(this.startOfWeek.getFullYear(),
                     this.startOfWeek.getMonth(),
                     this.startOfWeek.getDate() + i).getDate()
          } </span>
        ))}<br />
        {this.startOfWeek.getDate() + ' - ' + this.endOfWeek.getDate()}<br />
      </div>
    )
  }
}
