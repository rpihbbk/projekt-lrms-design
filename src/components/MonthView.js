import React, { Component } from 'react'

import '../css/MonthView.css'

export default class MonthView extends Component {
  constructor() {
    super()
    this._mockEvents = [
      {date: new Date("2017-03-20T13:16:53.928Z"), name: 'foo', time: '12.5'},
      {date: new Date("2017-03-20T13:18:53.928Z"), name: 'bar', time: '8'},
    ]
    this.events = this._mockEvents
    this.date = new Date()
    this.lastDateOfMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate()
    this.numberOfWeeks = Math.ceil((((new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay() + 6) % 7) + this.lastDateOfMonth) / 7)
    this.weeks = []
    for(let i = 0; i < this.numberOfWeeks;i++) {
      this.weeks[i] = []
      for(let j = 0; j < 7;j++) {
        this.weeks[i][j] = 0
      }
    }
    let i = 0
    for(let k = 1; k < this.lastDateOfMonth + 1; k++) {

      let l_day = new Date(this.date.getFullYear(), this.date.getMonth(), k)
      let l_day_of_week = l_day.getDay()-1 === -1 ? 6 : l_day.getDay()-1

      this.weeks[i][l_day_of_week] = l_day

      if(l_day_of_week === 6) i++
    }
  }

  compareWithoutTime(date1, date2) {
    return (date1.setHours(0, 0, 0, 0) === date2.setHours(0, 0, 0, 0))
  }

  render() {
    return (
      <div className="MonthView">
        <table>
          <tbody>
            <tr>
              <th>M</th>
              <th>T</th>
              <th>W</th>
              <th>T</th>
              <th>F</th>
              <th>S</th>
              <th>S</th>
            </tr>
            {this.weeks.map((item, i) => (
              <tr key={i}>
              {item.map((item, j) => (
                <td key={j}>
                  {item !== 0 ? (
                    <span style={{fontSize: 12}}>
                      {item.getDate()}
                      <br />
                      <span style={{textAlign: 'left'}}>
                      {this.events.map((event, i) => (
                        <span key={i} style={{fontSize: 8, }}>
                        {(this.compareWithoutTime(event.date, item) ? (event.time + ' ' + event.name) : null)}
                        <br />
                        </span>
                      ))}
                      </span>
                    </span>
                  ) : ''}
                </td>
              ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}
