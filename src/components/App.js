import React, { Component } from 'react'

import '../css/App.css'

import SideBar from './SideBar.js'
import MonthView from './MonthView.js'
import WeekView from './WeekView.js'
import DayView from './DayView.js'

import Settings from './Settings.js'
import Information from './Information.js'
import CLI from './cli.js'
import DB from './db.js'

const views = [
  {name: 'Ansichten', _name: 'views', icon: 'today', className: 'title no_hover', id: 0},
  {name: 'Monat', url: '/interface/month', _name: 'month', icon: '', className: '', id: 1},
  {name: 'Woche', url: '/interface/week', _name: 'week', icon: '', className: '', id: 2},
  {name: 'Tag', url: '/interface/day', _name: 'day', icon: '', className: '', id: 3},
  {name: 'Einstellungen', url: '/interface/settings', _name: 'settings', icon: 'settings', className: 'title', id: 4},
  {name: 'Informationen', url: '/interface/information', _name: 'information', icon: 'info_outline', className: 'title', id: 5},
  {name: 'Admin Dashboard', _name: 'admin', icon: 'code', className: 'title no_hover', id: 6},
  {name: 'Konsole', url: '/interface/cli', _name: 'cli', icon: '', className: '', id: 7},
  {name: 'Datenbank', url: '/interface/db', _name: 'db', icon: '', className: '', id: 8},
]

export default class App extends Component {
  constructor(props) {
    super(props)

    this.view = this.getActive()
  }

  getActive() {
    return views
      .map(view => view._name === this.props.match.params.view ? view.id : -1)
      .filter(x => x !== -1)[0] || 0
  }

  render() {
    return (
      <div className="App">
      <SideBar views={views} active={this.getActive()} />
        <div className="Main">
          {((v) => {
            if(v === 'month'       ) return <MonthView />
            if(v === 'week'        ) return <WeekView />
            if(v === 'day'         ) return <DayView />
            if(v === 'settings'    ) return <Settings />
            if(v === 'information' ) return <Information />
            if(v === 'cli'       ) return <CLI />
            if(v === 'db'       ) return <DB />
            if(v === undefined) return <MonthView />
            return <span>{'Something went wrong'}</span>
          })(this.props.match.params.view)}
        </div>
      </div>
    )
  }
}
