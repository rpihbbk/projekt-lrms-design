import React, { Component } from 'react'

import '../css/Settings.css'

export default class Settings extends Component {
  // constructor(props) {
  //   super(props)
  // }

  save() {

  }

  load() {

  }

  reset() {

  }

  render() {
    return (
      <div className="settings">
        <h3 className="title">Einstellungen</h3>
        <hr className="title-hr" />

        <h5 className="title-small">Aussehen:</h5>
        <span>
          Farbschema:
          <select style={{marginLeft: '12px'}} name="light">
            <option value="light">Hell</option>
            <option value="dark">Dunkel</option>
          </select>
        </span>
      </div>
    )
  }
}
