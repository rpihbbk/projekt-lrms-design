export default (cmd, args, flags, env) => {
  switch (true) {
    case !!flags.h:
      env.println('Help page for \'help\': \n\n Syntax: help [flag] \n\n flags:\n \'-h\': open the help page');
      break;
    default:
      env.println('The following commands are available:')
  }
}
