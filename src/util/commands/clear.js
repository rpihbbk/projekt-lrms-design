export default (cmd, args, flags, env) => {
  switch (true) {
    case !!flags.h:
      env.println('Help page for \'clear\': \n\n Syntax: Clear [flag] \n\n flags:\n \'-h\': open the help page');
      break;
    default:
      env.setHistory([])
  }
}
