export default (cmd, args, flags, env) => {
  let data = {
    'help': 'this is the man page for \'help\'.\n\n\'help\' is a command to show all available commands in the command prompt.\n use the flag -h for syntax details',
    'clear': 'this is the man page for \'clear\'.\n\n\'clear\' is a command to clear the command prompt.\n use the flag -h for syntax details',
    'man': 'this is the man page for \'man\'.\n\n\'man\' is a command that show a short manual for the command.\n use the flag -h for syntax details',
    'adduser': 'this is the man page for \'adduser\'.\n\n\'adduser\' is a command to creat new users. \n use the flag -h for syntax details',
    'deluser': 'this is the man page for \'deluser\'.\n\n\'deluser\' is a command to delete existing users. \n use the flag -h for syntax details',
    'listuser': 'this is the man page for \'listuser\'.\n\n\'listuser\' is a command list one or more users with ther informations. \n use the flag -h for syntax details',
    'moduser': 'this is the man page for \'moduser\'.\n\n\'moduser\' is a command to edit a user. \n use the flag -h for syntax details'
  }
  switch (true) {
    case !!flags.h:
      env.println('Help page for \'man\': \n\n Syntax: man [flag] \n\n flags:\n \'-h\': open the help page');
      break;
    case !!data[args[0]]:
      env.println(data[args[0]]);
      break;
    case !args[0] && !flags.h:
      env.println('usage: man [man page]')
      break;
    default:
      env.println('no man page found for \'' + args[0] + '\'')
  }
}
