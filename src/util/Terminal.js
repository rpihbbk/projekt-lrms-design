import clear from './commands/clear.js'
import help from './commands/help.js'
import man from './commands/man.js'
import adduser from './commands/user/adduser.js'
import deluser from './commands/user/deluser.js'
import moduser from './commands/user/moduser.js'
import listuser from './commands/user/listuser.js'

let _createArgsFromObj = (args) =>
  Object.keys(args).map(arg => isNaN(arg) ? '--' + arg + args[arg] : args[arg]).join(' ')

let _createFlagsFromObj = (flags) =>
  (Object.keys(flags).length === 0 ? '' : ' -') + Object.keys(flags).join('')

let alias = {
  'la': 'ls -a'
}

let commands = {
  'clear': clear,
  'help': help,
  'man': man,
  'adduser': adduser,
  'deluser': deluser,
  'moduser': moduser,
  'listuser': listuser
}

let parser = (command, env) => {
  let parse = (command) => {
    let tokens = command.split(/ +/)
    let name = tokens.shift()
    let args = {}
    let flags = {}
    let anonArgPos = 0

    while(tokens.length > 0) {
      const token = tokens.shift()
      if(token[0] === '-') {
        if(token[1] === '-') {
          const next = tokens.shift()
          args[token.slice(2)] = next;
        } else {
          token.slice(1).split('').forEach(flag => flags[flag] = true)
        }
      } else {
        args[anonArgPos++] = token
      }
    }
    return { name, flags, command, args }
  }

  let did_run = false

  let run = (l_obj) => {
    Object.keys(commands).forEach(key => {
      if(key === l_obj.name) commands[key](l_obj.command, l_obj.args, l_obj.flags, env)
    })
  }

  let checkAlias = (l_obj) => {
    Object.keys(alias).forEach(key => {
      if(key === l_obj.name) {
        let x = alias[key] +
        _createArgsFromObj(l_obj.args) +
        _createFlagsFromObj(l_obj.flags)
        run(parse(x))
        did_run = true
      }
    })
  }

  checkAlias(parse(command))

  if(!did_run) run(parse(command))
  did_run = false
}

export { parser }
