import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Header from './components/Header.js'
import App from './components/App'
import Login from './components/Login'
import './index.css'

ReactDOM.render((
  <Router>
    <div>
    <Header />
      <Route exact path="/" component={Login} />
      <Route exact path="/interface/" component={App} />
      <Route exact path="/interface/:view" component={App} />
      {/*<Route exact path="/interface/settings" component={App} />*/}
      {/*<Route exact path="/interface/information" component={App} />*/}
    </div>
  </Router>
  ),
  document.getElementById('root')
)
